// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <QSqlDatabase>
#include <QDateTime>
#include <optional>

struct PresenceData
{
    enum Availability : quint8 {
        Unavailable,
        Available,
        Away,
        ExtendedAway,
        Chat,
        DoNotDisturb,
    };

    QDateTime time;
    QString jid;
    QString resource;
    Availability availability;
    QDateTime lastOnline;
};

class Database
{
public:
    Database();

    void openDatabase();
    void insertData(PresenceData &&data);

private:
    std::optional<QSqlDatabase> m_database;
};
