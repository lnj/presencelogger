// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "database.h"

#include <QDebug>
#include <QDir>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlField>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QStandardPaths>

#define PRESENCES_TABLE_ u"presences"
constexpr QStringView PRESENCES_TABLE = PRESENCES_TABLE_;

QSqlField createSqlField(const QString &key, const QVariant &val)
{
    QSqlField field(key, val.type());
    field.setValue(val);
    return field;
}

void prepareQuery(QSqlQuery &query, const QString &sql)
{
    if (!query.prepare(sql)) {
        qDebug() << "Failed to prepare query:" << sql;
        qFatal("QSqlError: %s", qPrintable(query.lastError().text()));
    }
}

void execQuery(QSqlQuery &query)
{
    if (!query.exec()) {
        qDebug() << "Failed to execute query:" << query.executedQuery();
        qFatal("QSqlError: %s", qPrintable(query.lastError().text()));
    }
}

void execQuery(QSqlQuery &query, const QString &sql)
{
    prepareQuery(query, sql);
    execQuery(query);
}

void execQuery(QSqlQuery &query,
               const QString &sql,
               const QVector<QVariant> &bindValues)
{
    prepareQuery(query, sql);

    for (const auto &val : bindValues) {
        query.addBindValue(val);
    }

    execQuery(query);
}

void execQuery(QSqlQuery &query,
                      const QString &sql,
                      const QMap<QString, QVariant> &bindValues)
{
    prepareQuery(query, sql);

    const QStringList bindKeys = bindValues.keys();
    for (const auto &key : bindKeys)
        query.bindValue(key, bindValues.value(key));

    execQuery(query);
}

Database::Database()
{
    if (m_database) {
        m_database->close();
    }
}

void Database::openDatabase()
{
    Q_ASSERT(!m_database.has_value());

    m_database = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"), QStringLiteral("main"));
    if (!m_database->isValid()) {
        qFatal("Cannot add database: %s", qPrintable(m_database->lastError().text()));
    }

    const auto writeDir = QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (!writeDir.mkpath(QLatin1String("."))) {
        qFatal("Failed to create writable directory at %s", qPrintable(writeDir.absolutePath()));
    }

    // Ensure that we have a writable location on all devices.
    const auto fileName = writeDir.absoluteFilePath(QStringLiteral("data.sqlite"));
    // open() will create the SQLite database if it doesn't exist.
    m_database->setDatabaseName(fileName);
    if (!m_database->open()) {
        qFatal("Cannot open database: %s", qPrintable(m_database->lastError().text()));
    }

    QSqlQuery query(*m_database);
    execQuery(query, QStringLiteral(
        "CREATE TABLE IF NOT EXISTS '" PRESENCES_TABLE_ "' ("
        "time INTEGER NOT NULL,"
        "jid TEXT NOT NULL,"
        "resource TEXT,"
        "availability INTEGER NOT NULL,"
        "lastOnline INTEGER"
        ")"));
}

void Database::insertData(PresenceData &&data)
{
    Q_ASSERT(m_database.has_value());
    auto record = m_database->record(QStringLiteral("presences"));
    record.setValue(QStringLiteral("time"), data.time.toMSecsSinceEpoch());
    record.setValue(QStringLiteral("jid"), data.jid);
    record.setValue(QStringLiteral("resource"), data.resource);
    record.setValue(QStringLiteral("availability"), quint8(data.availability));
    record.setValue(QStringLiteral("lastOnline"), data.lastOnline.toMSecsSinceEpoch());

    QSqlQuery query(*m_database);
    execQuery(query, m_database->driver()->sqlStatement(
        QSqlDriver::InsertStatement,
        PRESENCES_TABLE.toString(),
        record,
        false
    ));
}
