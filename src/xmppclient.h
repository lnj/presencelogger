// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <QXmppClient.h>
#include <QTimer>
#include "database.h"

class XmppClient : public QXmppClient
{
    Q_OBJECT

public:
    XmppClient(QObject *parent = nullptr);
    ~XmppClient();

    void login();
    void initialLogin(const QString &jid);

private:
    Q_SLOT void onConnected();
    Q_SLOT void onDisconnected();
    Q_SLOT void onPresenceReceived(const QXmppPresence &presence);
    Q_SLOT void onDataTimer();

    void readSettings();
    void saveSettings();

    QString m_jid;
    QString m_password;
    QVector<PresenceData> m_presenceDataCache;
    QTimer m_saveTimer;
    Database m_database;
};
