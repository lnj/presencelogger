// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "xmppclient.h"
#include <iostream>
#include <QDateTime>
#include <QSettings>
#include <QCoreApplication>
#include <QXmppUtils.h>

QStringView availabilityTypeToString(QXmppPresence::AvailableStatusType type)
{
    using T = QXmppPresence::AvailableStatusType;
    switch (type) {
    case T::Away:
        return u"away  ";
    case T::Chat:
        return u"chat  ";
    case T::DND:
        return u"dnd   ";
    case T::Online:
        return u"online";
    case T::XA:
        return u"xa    ";
    default:
        return u"      ";
    }
}

PresenceData::Availability presenceToAvailability(const QXmppPresence &presence)
{
    switch (presence.type()) {
    case QXmppPresence::Available:
        switch (presence.availableStatusType()) {
        case QXmppPresence::AvailableStatusType::Away:
            return PresenceData::Away;
        case QXmppPresence::AvailableStatusType::Chat:
            return PresenceData::Chat;
        case QXmppPresence::AvailableStatusType::DND:
            return PresenceData::DoNotDisturb;
        case QXmppPresence::AvailableStatusType::Online:
            return PresenceData::Available;
        case QXmppPresence::AvailableStatusType::XA:
            return PresenceData::ExtendedAway;
        default:
            return PresenceData::Available;
        }
    default:
        return PresenceData::Unavailable;
    }
}

XmppClient::XmppClient(QObject *parent)
    : QXmppClient(parent)
{
    connect(this, &QXmppClient::presenceReceived, this, &XmppClient::onPresenceReceived);
    connect(this, &QXmppClient::connected, this, &XmppClient::onConnected);
    connect(this, &QXmppClient::disconnected, this, &XmppClient::onDisconnected);

#if QT_VERISON >= QT_VERSION_CHECK(5, 12, 0)
    m_saveTimer.callOnTimeout(this, &XmppClient::onDataTimer);
#else
    connect(&m_saveTimer, &QTimer::timeout, this, &XmppClient::onDataTimer);
#endif
    m_saveTimer.setSingleShot(false);
    m_saveTimer.setInterval(5000);
}

XmppClient::~XmppClient()
{
    disconnectFromServer();
    m_saveTimer.stop();
    onDataTimer();
}

void XmppClient::login()
{
    m_database.openDatabase();
    readSettings();

    if (m_jid.isEmpty() || !m_jid.contains(u'@') || m_jid.size() < 3 || m_password.isEmpty()) {
        qFatal("No valid JID or password configured! Call with --login to save credentials.");
    }

    qDebug() << "Connecting as" << m_jid;
    connectToServer(m_jid, m_password);
}

void XmppClient::initialLogin(const QString &jid)
{
    std::cout << "Logging in as " << jid.toStdString() << ".\n";
    std::string password;
    while (password.empty()) {
        std::cout << "Password: ";
        std::cin >> password;
    }

    m_jid = jid;
    m_password = QString::fromStdString(password);
    saveSettings();
}

void XmppClient::onConnected()
{
    qDebug() << "Connected.";
    m_saveTimer.start();
}

void XmppClient::onDisconnected()
{
    qDebug() << "Disconnected";
    m_saveTimer.stop();
    onDataTimer();
    QCoreApplication::quit();
}

void XmppClient::onPresenceReceived(const QXmppPresence &presence)
{
    if (presence.from() == configuration().jid()) {
        return;
    }

    switch (presence.type()) {
    case QXmppPresence::Available:
    case QXmppPresence::Unavailable: {
        PresenceData data {
            QDateTime::currentDateTimeUtc(),
            QXmppUtils::jidToBareJid(presence.from()),
            QXmppUtils::jidToResource(presence.from()),
            presenceToAvailability(presence),
            presence.lastUserInteraction(),
        };

        const auto available = presence.type() == QXmppPresence::Available;

        auto resource = data.resource;
        resource.resize(20, u' ');
        auto jid = data.jid;
        jid.resize(30, u' ');

        const auto availabilityType = availabilityTypeToString(presence.availableStatusType());

        qDebug().noquote()
                << QDateTime::currentDateTimeUtc().toString(u"yyyy-MM-dd hh:mm:ss")
                << (available ? "[ ONLINE  ]" : "[ OFFLINE ]")
                << jid
                << resource
                << (available ? availabilityType : u"      ")
                << (data.lastOnline.isValid() ? data.lastOnline.toString(u"yyyy-MM-dd hh:mm:ss") : QString(u' ').repeated(20));

        m_presenceDataCache << std::move(data);
    }
    default:
        // ignore
        break;
    }
}

void XmppClient::onDataTimer()
{
    if (!m_presenceDataCache.empty()) {
        for (auto &data : m_presenceDataCache) {
            m_database.insertData(std::move(data));
        }

        m_presenceDataCache.clear();
    }
}

void XmppClient::readSettings()
{
    QSettings settings;
    m_jid = settings.value(QStringLiteral("auth/jid")).toString();
    m_password = QString::fromUtf8(
                QByteArray::fromBase64(
                    settings.value(QStringLiteral("auth/password")).toByteArray()));
}

void XmppClient::saveSettings()
{
    QSettings settings;
    settings.setValue(QStringLiteral("auth/jid"), m_jid);
    settings.setValue(QStringLiteral("auth/password"), m_password.toUtf8().toBase64());
}
