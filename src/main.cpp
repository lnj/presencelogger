// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <QCoreApplication>
#include "xmppclient.h"
#include "commandlineparser.h"

constexpr QStringView APPLICATION_NAME = u"presencelogger";
constexpr QStringView APPLICATION_VERSION = u"" PROJECT_VERSION;
constexpr QStringView ORGANISATION_NAME = u"kaidan";
constexpr QStringView ORGANISATION_DOMAIN = u"kaidan.im";

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    app.setApplicationName(APPLICATION_NAME.toString());
    app.setApplicationVersion(APPLICATION_VERSION.toString());
    app.setOrganizationName(ORGANISATION_NAME.toString());
    app.setOrganizationDomain(ORGANISATION_DOMAIN.toString());

    // parse command line options
    CommandLineParser parser;
    auto result = parser.process(app);

    XmppClient client;
    if (const auto login = std::get_if<CommandLineParser::Login>(&result)) {
        client.initialLogin(login->jid);
        return 0;
    } else {
        client.login();
    }

    return app.exec();
}
